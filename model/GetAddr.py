# -*- encoding: utf-8 -*-
"""
@File    :   Get.py
@Time    :   2022-11-04 16:41
@Author  :   坐公交也用券
@Version :   1.0
@Contact :   faith01238@hotmail.com
@Homepage : https://liumou.site
@Desc    :   获取IP列表
"""
import re
import threading
import time
from multiprocessing import Manager
from random import randint

import requests
from ColorInfo import ColorLogger
from bs4 import BeautifulSoup


class GetAddress:
	def __init__(self):
		"""
		初始化

		"""
		self.cookies = None
		self.logger = ColorLogger(file='GetAddress')
		self.response = None
		self.url_list = ['www.github.com']
		self.headers = {}
		# 结果
		self.result_dic = {}
		self.text = ''
		self.params = {}

	def chinaz(self, url, dic_):
		"""
		使用http://mip.chinaz.com/网址查询接口
		:return:
		"""
		headers = {
			'Connection': 'keep-alive',
			'Cache-Control': 'max-age=0',
			'Upgrade-Insecure-Requests': '1',
			'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36',
			'Origin': 'http://mip.chinaz.com',
			'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
			'Referer': 'http://mip.chinaz.com/?query=arch.liumou.site',
			'Accept-Language': 'zh-CN,zh;q=0.9', }

		params = {
			'query': '%s' % url,
		}

		data = {
			'query': '%s' % url,
		}
		pattern = re.compile(r'\d+\.\d+\.\d+\.\d+')
		u = 'http://mip.chinaz.com/'
		response = requests.post(u, params=params, headers=headers, data=data, verify=False)
		try:
			soup = BeautifulSoup(response.content, 'html.parser')
			ip = pattern.search(str(soup.text)).group()
			self.logger.info('[ %s ] query was successful <- chinaz: ' % url, ip)
			self.result_dic[url] = ip
			dic_[url] = ip
		except Exception as e:
			self.logger.error(str(e))
			return False
		return True

	def t(self, dic_):
		"""
		使用多线程查询
		:return:
		"""
		threads = []
		for url in self.url_list:
			t = threading.Thread(target=self.get_ip, args=(url, dic_,))
			t.start()
			threads.append(t)
		for t in threads:
			t.join()

	def create(self):
		"""
		创建映射表
		:return:
		"""
		for i in self.result_dic:
			txt = "%s %s" % (self.result_dic[i], i)
			print(txt)
			self.text = str(self.text) + txt.replace("\n", '') + '\n'

	def cc(self):
		self.url_list = ['avatars1.githubusercontent.com', 'www.github.com', 'camo.githubusercontent.com',
		                 'avatars7.githubusercontent.com', 'raw.github.com', 'avatars8.githubusercontent.com',
		                 'avatars0.githubusercontent.com', 'avatars3.githubusercontent.com',
		                 'avatars4.githubusercontent.com', 'avatars5.githubusercontent.com',
		                 'marketplace-screenshots.githubusercontent.com', 'gist.githubusercontent.com',
		                 'cloud.githubusercontent.com', 'repository-images.githubusercontent.com',
		                 'user-images.githubusercontent.com', 'assets-cdn.github.com', 'avatars2.githubusercontent.com',
		                 'raw.githubusercontent.com', 'avatars6.githubusercontent.com', 'desktop.githubusercontent.com',
		                 'github.com', 'pypi.org', 'hub.docker.com', 'www.python.org']

	def head_(self):
		"""
		用于http://dbcha.com/
		:return:
		"""
		self.headers = {
			'Connection': 'keep-alive',
			'Cache-Control': 'max-age=0',
			'Upgrade-Insecure-Requests': '1',
			'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36',
			'Origin': 'http://dbcha.com',
			'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
			'Referer': 'http://dbcha.com/?t=1667576995',
			'Accept-Language': 'zh-CN,zh;q=0.9',
		}
		t = randint(1667577002, 9667577002)
		self.params = {
			't': '%s' % t,
		}

	def get_ip(self, url, dic_):
		self.head_()
		data = {
			'name': '%s' % url,
			'time': 'A',
			'button': '立即查询',
		}
		lv = randint(1667576997, 2667576997)
		self.cookies = {
			'Hm_lvt_3725bc1d21b69fea2abcfe99ad31b084': '%s' % lv,
			'Hm_lpvt_3725bc1d21b69fea2abcfe99ad31b084': '%s' % lv,
		}
		u = 'http://dbcha.com/'
		ip = ''
		self.response = requests.post(u, params=self.params, cookies=self.cookies,
		                              headers=self.headers, data=data, verify=False)
		try:
			soup = BeautifulSoup(self.response.content, 'html.parser')
			ip = soup.find('table').text.split('ip')[-1]
		except Exception as e:
			self.logger.error(str(e))
			self.logger.warning("dbcha query Failed, Use chinaz Api")
			self.chinaz(url=url, dic_=dic_)
		# 开始判断格式
		ip = str(ip).replace('\n', '')
		if len(str(ip).split('.')) == 4:
			self.logger.info('[ %s ] query was successful: ' % url, ip)
			self.result_dic[url] = ip
			dic_[url] = ip
			return True
		return False

	def test(self):
		"""
		访问测试
		:return:
		"""
		ok_dic = {}
		for url in self.result_dic:
			ip = self.result_dic[url]
			http_ = "https://%s:443" % ip
			try:
				res = requests.get(url=http_, headers=self.headers, timeout=5)
				if res.status_code == 200:
					self.logger.info('Test Is OK: ', ip)
					ok_dic[url] = ip
				else:
					self.logger.warning("Test is Not ok: ", ip)
			except Exception as e:
				self.logger.error(str(e))
		self.logger.info('Test Done')
		self.logger.info('Number of passes: ', len(ok_dic))
		s = len(self.result_dic) - len(ok_dic)
		self.logger.warning("Number of failures: ", s)

	def start(self):
		"""
		开始获取
		:return:
		"""
		s_ = time.time()
		dic_ = Manager().dict()
		self.result_dic = dic_
		self.cc()
		self.t(dic_=dic_)
		self.logger.info('Query End!')
		self.logger.debug('Number of successful queries: ', len(self.result_dic))
		# self.test()
		if len(self.result_dic) >= 1:
			self.create()
			e_ = time.time()
			self.logger.info("Total time: %s second" % (e_ - s_))
			return True
		e_ = time.time()
		self.logger.error("Total time: %s second" % (e_ - s_))
		return False


if __name__ == "__main__":
	get = GetAddress()
	get.start()
