# -*- encoding: utf-8 -*-
"""
@File    :   FM.py
@Time    :   2022-11-04 16:41
@Author  :   坐公交也用券
@Version :   1.0
@Contact :   faith01238@hotmail.com
@Homepage : https://liumou.site
@Desc    :   文件管理
"""
from os import path
from shutil import copy2

from ColorInfo import ColorLogger


class Fm:
	def __init__(self, src, dst):
		"""

		:param src:
		:param dst:
		"""
		self.dst = dst
		self.src = src
		self.logger = ColorLogger()
		self.txt = ''

	def back(self):
		if path.isfile(self.dst):
			self.logger.info("Backed up: ", self.dst)
			return True
		else:
			self.logger.warning('Not backed up: ', self.dst)
			self.logger.debug('Backing up...')
		try:
			copy2(src=self.src, dst=self.dst)
		except Exception as e:
			self.logger.error("Backup failed: ", str(e))
			return False
		return True

	def read(self, file=None):
		"""
		读取源内容
		:return:
		"""
		if file is None:
			file = self.dst
		self.logger.debug('Read File : ', file)
		try:
			with open(file=file, mode='r', encoding='utf-8') as r:
				self.txt = r.read()
				r.close()
		except Exception as e:
			self.logger.error(file, ' - read failure: ', str(e))
			return False
		return True

	def copy(self, src):
		"""
		复制文件
		:param src:
		:return:
		"""
		self.logger.debug(src, "Copy To -> ", self.src)
		try:
			copy2(src=src, dst=self.src)
		except Exception as e:
			self.logger.error('Failed to copy the file, please check the permission: ', str(e))
			return False
		self.logger.info('Copy succeeded')
		return True
