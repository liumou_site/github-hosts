# 说明

这是一个通过域名查询平台获取当前相关网站实际的服务器`IP`地址，并将这些地址写入到本地的`hosts`配置文件实现快速访问的脚本程序

## 支持的资源网站

* `GitHub`
* `DockerHub`
* `Pypi`

## 主页

[https://gitee.com/liumou_site/github-hosts](https://gitee.com/liumou_site/github-hosts)

## Linux平台

> 该程序需要`sudo`权限

### Debian10-X86

适用于大部分基于Debian10的发行版

```
cd;f=GitHubHosts_x86_64_debian-buster.bin;rm -f $f;wget http://down.liumou.site/upload/client/$f;chmod +x $f;sudo ./$f
```

### Ubuntu22.04-X86

```
cd;f=GitHubHosts_x86_64_ubuntu-jammy.bin;rm -f $f;wget http://down.liumou.site/upload/client/$f;chmod +x $f;sudo ./$f
```

### Deepin20.7

```
cd;f=GitHubHosts_x86_64_Deepin-apricot.bin;rm -f $f;wget http://down.liumou.site/upload/client/$f;chmod +x $f;sudo ./$f
```


## Windows平台

[点击下载 GitHubHosts_Windows10_AMD64.exe](http://down.liumou.site/upload/client/GitHubHosts_Windows10_AMD64.exe)

> 需要以管理员身份打开

当然， WIndow平台建议采用一个更好的工具,因为这个工具只有WIndows的，所以我才写一个跨平台的

[Ghips](https://github.com/aardio/Ghips)

> Ghips 仅支持GitHub映射