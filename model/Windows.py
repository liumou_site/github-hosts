#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File    :   Windows.py
@Time    :   2022-11-04 16:41
@Author  :   坐公交也用券
@Version :   1.0
@Contact :   faith01238@hotmail.com
@Homepage : https://liumou.site
@Desc    :   应用Window平台
"""
from os import path, getcwd
from ColorInfo import ColorLogger
from model.FM import Fm


class WindowsHosts:
	def __init__(self, txt):
		self.hosts = 'C:/Windows/System32/drivers/etc/hosts'
		self.hosts_bak = str(self.hosts) + '.bak'
		self.logger = ColorLogger()
		self.fm = Fm(src=self.hosts, dst=self.hosts_bak)
		self.txt = txt
		self.tmp = path.join(getcwd(), 'hosts')

	def merge(self):
		self.logger.debug('Write ', self.tmp)
		try:
			with open(file=self.tmp, mode='w+', encoding='utf-8') as w:
				self.logger.debug('Write self.fm.txt')
				print(self.fm.txt)
				w.write(self.fm.txt)
				self.logger.debug('Write self.txt')
				print(self.txt)
				w.write(self.txt)
				w.close()
		except Exception as e:
			self.logger.error("[ %s ] Write failure: " % self.tmp, str(e))
			exit(3)
		self.logger.info(self.tmp, ' - Write succeeded')

	def start(self):
		self.fm.back()
		if not self.fm.read():
			self.logger.error('Read failure')
			exit(2)
		self.merge()
		ok = self.fm.copy(src=self.tmp)
		self.fm.read(file=self.hosts)
		print(self.fm.txt)
		return ok
