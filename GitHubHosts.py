# -*- encoding: utf-8 -*-
"""
@File    :   GitHubHosts.py
@Time    :   2022-11-04 16:40
@Author  :   坐公交也用券
@Version :   1.0
@Contact :   faith01238@hotmail.com
@Homepage : https://liumou.site
@Desc    :   GitHub自动更新Hosts程序
"""
import sys
from argparse import ArgumentParser
from model.GetAddr import GetAddress
from model.Windows import WindowsHosts
from model.Linux import LinuxHosts
from PyQt5.QtWidgets import QMessageBox, QApplication, QWidget
import platform
from ColorInfo import ColorLogger


class GitHubHosts(QWidget):
	def __init__(self):
		super().__init__()
		self.run()

	def run(self):
		logger = ColorLogger(class_name=self.__class__.__name__)
		get = GetAddress()
		if get.start():
			h = WindowsHosts(txt=get.text)
			if platform.system().lower() == 'linux':
				h = LinuxHosts(password='password', txt=get.text)
			ok = h.start()
			if ok:
				logger.info(msg='Config Done')
				QMessageBox.information(self, '配置结果', '配置成功')
			else:
				logger.error(msg='Config Failed')
				QMessageBox.warning(self, '配置结果', '配置失败')
				exit(1)
		else:
			print('Get failed...')
			exit(2)
		exit(0)


if __name__ == "__main__":
	app = QApplication(sys.argv)
	arg = ArgumentParser(description='version: 2.5', prog="国外网站服务器映射程序")
	arg.add_argument('-p', '--passwd', type=str, help='设置OS密码, 默认：出厂密码', default='1', required=False)
	args = arg.parse_args()
	pd = args.passwd
	w = GitHubHosts()
	sys.exit(app.exec_())
