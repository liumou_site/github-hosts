# -*- encoding: utf-8 -*-
import re

import requests
from ColorInfo import ColorLogger
from bs4 import BeautifulSoup

logger = ColorLogger(file='1.txt')
headers = {
	'Connection': 'keep-alive',
	'Cache-Control': 'max-age=0',
	'Upgrade-Insecure-Requests': '1',
	'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36',
	'Origin': 'http://mip.chinaz.com',
	'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
	'Referer': 'http://mip.chinaz.com/?query=arch.liumou.site',
	'Accept-Language': 'zh-CN,zh;q=0.9',}

params = {
	'query': 'www.github.com',
}

data = {
	'query': 'www.github.com',
}
pattern = re.compile(r'\d+\.\d+\.\d+\.\d+')

response = requests.post('http://mip.chinaz.com/', params=params, headers=headers, data=data, verify=False)
try:
	soup = BeautifulSoup(response.content, 'html.parser')
	# print(soup.text)
	ip = pattern.search(str(soup.text)).group()
	print(ip)
	# logger.info(ip)
except Exception as e:
	logger.error(str(e))

