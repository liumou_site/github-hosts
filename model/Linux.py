#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File    :   Linux.py
@Time    :   2022-11-04 16:41
@Author  :   坐公交也用券
@Version :   1.0
@Contact :   faith01238@hotmail.com
@Homepage : https://liumou.site
@Desc    :   应用Linux平台
"""
from sys import exit

from plbm import ColorLogger, ComMand, Jurisdiction

from model.FM import Fm
from os import path, getcwd


class LinuxHosts:
	def __init__(self, password, txt):
		"""

		:param password:
		:param txt:
		"""
		self.hosts = '/etc/hosts'
		self.hosts_bak = str(self.hosts) + '.bak'
		self.logger = ColorLogger()

		ju = Jurisdiction(passwd=password)
		if not ju.verification(name="LinuxHosts"):
			self.logger.error('No permission')
			exit(2)
		self.cmd = ComMand(password=password)
		self.fm = Fm(src=self.hosts, dst=self.hosts_bak)
		self.txt = txt
		self.tmp = path.join(getcwd(), 'hosts')

	def merge(self):
		try:
			with open(file=self.tmp, mode='w+', encoding='utf-8') as w:
				print('Write')
				print(self.fm.txt)
				w.write(self.fm.txt)
				print('Write')
				print(self.txt)
				w.write(self.txt)
				w.close()
		except Exception as e:
			self.logger.error("[ %s ] Write failure: " % self.tmp, str(e))
			exit(3)
		self.logger.info(self.tmp, ' - Write succeeded')

	def start(self):
		self.fm.back()
		if not self.fm.read():
			self.logger.error('读取失败')
			exit(2)
		self.merge()
		ok = self.fm.copy(src=self.tmp)
		self.fm.read(self.hosts)
		print(self.fm.txt)
		return ok
